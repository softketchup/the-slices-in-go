// C:\Go\src\github.com\headfirstgo\average2

// average2 вычисляет среднее значение.
package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

func average(numbers ...float64) float64 {

	var sum float64 = 0

	for _, number := range numbers {
		sum += number
	}

	return sum / float64(len(numbers))
}

func main() {
	arguments := os.Args[1:]

	var numbers []float64

	for _, argument := range arguments {
		number, err := strconv.ParseFloat(argument, 64)

		if err != nil {
			log.Fatal(err)
		}

		numbers = append(numbers, number)
	}

	// sampleCount := float64(len(arguments))
	// fmt.Printf("Average: %0.2f\n", sum/sampleCount)
	fmt.Printf("Average: %0.2f\n", average(numbers...))
}
